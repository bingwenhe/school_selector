function plot_grade() {

    var class_elem = document.getElementById('selectedClass')
    var class_var = class_elem.options[ class_elem.selectedIndex ].text

    var subject_elem = document.getElementById('selectedSubject')
    var subject_var = subject_elem.options[ subject_elem.selectedIndex ].text

    var method_elem = document.getElementById('selectedMethod')
    var method_var = method_elem.options[ method_elem.selectedIndex ].text

    var page_title = class_var + " - " + subject_var + " - " + method_var
    console.log(page_title)

    var xhr = new XMLHttpRequest();
//    var url = "http://localhost:5000/bingwen_sum?final_year=ak9_amne"
    var url = "http://localhost:5000/fetch_grade"
        url += "?method=" + method_var
        url += "&class_year=" + class_var
        url += "&subject=" + subject_var

    xhr.open("GET", url, async=true);
    xhr.onload = function (e) {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
          } else {
            console.error(xhr.statusText);
          }
        }
        var jsonData = JSON.parse(xhr.responseText);

        hoverFrame = document.getElementById('hoverinfo'),

        /* Current Plotly.js version */
        console.log(Plotly.version);

        var myPlot = document.getElementById('tester'),
            d3 = Plotly.d3,
            N = 16,
            layout = {
                hovermode: 'closest',
                title: page_title,
                xaxis: {
                    fixedrange: true,
                    tickformat: 'd'
                },
                yaxis: {
                    fixedrange: true
                },
                margin: {
                    l: 15,
                    r: 10,
                    b: 15,
                    t: 25,
                    pad: 0
                  }
            };
        schoolList = []
        for ( var schoolData in jsonData) {
            schoolList.push(
                {
                    x: jsonData[schoolData]['year_list'],
                    y: jsonData[schoolData]['grade_list'],
                    type: 'scatter',
                    name: schoolData,
                    hoverinfo: "y"
                }
            )
        }
        totalTrace = schoolList.length
        totalXTicker = schoolList[0].x.length

        Plotly.newPlot('tester', schoolList, layout);

        myPlot.on('plotly_hover', function (data) {
            var points = data.points[0];
            console.log(points)
            pointNum = points.pointNumber;
            // 1. vertical
            // 2. whole trace
            const reducer1 = (acc, cur) => acc.concat([{ curveNumber: cur, pointNumber: pointNum }])
            verticalData = [...Array(totalTrace).keys()].reduce( reducer1, [] )
            const reducer2 = (acc, cur) => acc.concat([{ curveNumber: points.curveNumber, pointNumber: cur }])
            traceData = [...Array(totalXTicker).keys()].reduce( reducer2, [] )
            hoverData = verticalData.concat(traceData);
            // hoverData = verticalData
            hoverData = traceData
            Plotly.Fx.hover('tester', hoverData);
            var infotext = points.data.name;
            hoverFrame.innerHTML = infotext
        });
      };
    xhr.send();
}

