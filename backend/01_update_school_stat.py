import pandas as pd
import glob
import helper
import parameter_configuration as para_conf
from collections import Counter
import csv
from classes.school_grade_class import SchoolGradeSubject, SchoolGradeTotal
import time
import os

pd.set_option('display.width', 1000)
pd.set_option('display.max_rows', 15000)

BEST_CNT = 3
TOP_CNT = 12


def write_school_stat(analysis_data,
                      best_cnt_school_counter,
                      top_cnt_school_counter,
                      all_school_counter,
                      best_cnt=BEST_CNT, top_cnt=TOP_CNT):
    timestr = time.strftime("%Y%m%d_%H%M%S")

    class_name = analysis_data['class']
    class_type = analysis_data['type']

    school_best_cnt_file = f"{class_name}_{class_type}_best_{str(best_cnt)}.{timestr}.csv"
    school_top_cnt_file = f"{class_name}_{class_type}_top_{str(top_cnt)}.{timestr}.csv"
    all_school_file = f"{class_name}_{class_type}_all.{timestr}.csv"

    with open(os.path.join(para_conf.RESULT_FOLDER, school_best_cnt_file), 'w') as csv_file:
        fieldnames = ['school_kod', 'school_name', 'best_cnt', 'total_cnt', 'percent']
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()
        for school_id, value in best_cnt_school_counter:
            total_cnt = helper.find_school_total_count_by_id(school_id, all_school_counter)
            percent = int(value / total_cnt * 100)
            new_row = {
                'school_kod': school_id,
                'school_name': helper.find_school_name_by_id(school_id),
                'best_cnt': value,
                'total_cnt': total_cnt,
                'percent': percent}
            writer.writerow(new_row)

    with open(os.path.join(para_conf.RESULT_FOLDER, school_top_cnt_file), 'w') as csv_file:
        fieldnames = ['school_kod', 'school_name', 'top_cnt', 'total_cnt', 'percent']
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()
        for school_id, value in top_cnt_school_counter:
            total_cnt = helper.find_school_total_count_by_id(school_id, all_school_counter)
            percent = int(value / total_cnt * 100)
            new_row = {
                'school_kod': school_id,
                'school_name': helper.find_school_name_by_id(school_id),
                'top_cnt': value,
                'total_cnt': total_cnt,
                'percent': percent}
            writer.writerow(new_row)

    with open(os.path.join(para_conf.RESULT_FOLDER, all_school_file), 'w') as csv_file:
        fieldnames = ['count', 'school_kod', 'school_name']
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()
        for school_id, value in all_school_counter:
            new_row = {
                'school_kod': school_id,
                'school_name': helper.find_school_name_by_id(school_id),
                'count': value}
            writer.writerow(new_row)


def update_school_stat(profile):
    best_cnt = BEST_CNT
    top_cnt = TOP_CNT

    best_cnt_school = []
    top_cnt_school = []
    all_school = []

    all_csv_files_pattern = profile['file_pattern']
    all_csv_files = glob.glob(all_csv_files_pattern)
    grade_col = profile['grade_col']

    for csv_file in all_csv_files:
        print(csv_file)
        if profile['type'] == para_conf.GRADE_TYPE_SUBJECT:
            school_grade_obj = SchoolGradeSubject(csv_file,
                                                  grade_col=grade_col,
                                                  class_name=profile['class'],
                                                  grade_type=profile['type'],
                                                  best_cnt=best_cnt,
                                                  top_cnt=top_cnt)
        elif profile['type'] == para_conf.GRADE_TYPE_TOTAL:
            school_grade_obj = SchoolGradeTotal(csv_file,
                                                class_name=profile['class'],
                                                grade_col=grade_col,
                                                grade_type=profile['type'],
                                                best_cnt=best_cnt,
                                                top_cnt=top_cnt)

        school_grade_obj.populate_complete_school_list()
        school_grade_obj.reorganize_grade()
        school_grade_obj.populate_cnt()
        best_cnt_school += school_grade_obj.best_cnt_school
        top_cnt_school += school_grade_obj.top_cnt_school
        all_school += school_grade_obj.complete_school_list

    # Todo: heatmap plot Counter(all_schools)
    best_cnt_school_counter = Counter(best_cnt_school).most_common()
    top_cnt_school_counter = Counter(top_cnt_school).most_common()
    all_school_counter = Counter(all_school).most_common()

    write_school_stat(profile,
                      best_cnt_school_counter,
                      top_cnt_school_counter,
                      all_school_counter,
                      best_cnt=best_cnt,
                      top_cnt=top_cnt)


update_school_stat(para_conf.DATA_AK6_AMNE)
update_school_stat(para_conf.DATA_AK9_AMNE)
update_school_stat(para_conf.DATA_AK9_TOTAL)


