import pandas as pd
import glob
import helper
from collections import Counter
import csv
from abc import abstractmethod,ABCMeta


class SchoolGradeBase(metaclass=ABCMeta):
    def __init__(self, file_name, class_name, grade_col, grade_type, best_cnt, top_cnt):
        self.file_name = file_name
        self.grade_col = grade_col
        self.best_cnt = best_cnt
        self.top_cnt = top_cnt
        self.grade_type = grade_type
        self.class_name = class_name

        self.raw_grade_df = None
        self.grade_df = None
        self.complete_school_list = []
        self.year = helper.get_year_from_filename(self.file_name)
        self.best_cnt_school = []
        self.top_cnt_school = []

    def _populate_raw_grade(self):
        assert self.use_col
        one_df = pd.read_csv(self.file_name, sep=';', usecols=self.use_col)
        one_df['grade'] = one_df[self.grade_col].map(helper.numify)
        one_df.drop(self.grade_col, 1, inplace=True)
        self.raw_grade_df = one_df[(one_df['Kommun'] == 'Stockholm')]

    def populate_complete_school_list(self):
        self.complete_school_list = list(set(self.raw_grade_df['Skol-enhetskod']))


class SchoolGradeSubject(SchoolGradeBase):

    def __init__(self, file_name, class_name, grade_col, grade_type, best_cnt, top_cnt):
        super().__init__(file_name, class_name, grade_col, grade_type, best_cnt, top_cnt)
        self.use_col = ['Kommun', 'Skol-enhetskod', 'Ämne', self.grade_col]
        self._populate_raw_grade()

    def reorganize_grade(self):
        one_df = self.raw_grade_df.pivot_table(index='Skol-enhetskod', columns='Ämne', values='grade')
        final_df = one_df[['Svenska', 'Engelska', 'Matematik']].copy()
        final_df['year'] = self.year
        self.grade_df = final_df

    # 单科进过前best_cnt肯定留着，单科进过top_cnt存入Dataframe，之后分析
    def populate_cnt(self):
        sv_best, sv_top = helper.find_top(self.grade_df, col='Svenska', top_cnt=self.top_cnt, best_cnt=self.best_cnt)
        eng_best, eng_top = helper.find_top(self.grade_df, col='Engelska', top_cnt=self.top_cnt, best_cnt=self.best_cnt)
        math_best, math_top = helper.find_top(self.grade_df, col='Matematik', top_cnt=self.top_cnt, best_cnt=self.best_cnt)

        self.best_cnt_school = list(set(sv_best + eng_best + math_best))
        self.top_cnt_school = list(set(sv_top + eng_top + math_top))


class SchoolGradeTotal(SchoolGradeBase):

    def __init__(self, file_name, class_name, grade_col, grade_type, best_cnt, top_cnt):
        super().__init__(file_name, class_name, grade_col, grade_type, best_cnt, top_cnt)
        self.use_col = ['Kommun', 'Skol-enhetskod', self.grade_col]
        self._populate_raw_grade()

    def reorganize_grade(self):
        self.grade_df = self.raw_grade_df.copy()
        self.grade_df.set_index('Skol-enhetskod', inplace=True)

    # 单科进过前best_cnt肯定留着，单科进过top_cnt存入Dataframe，之后分析
    def populate_cnt(self):
        self.best_cnt_school, self.top_cnt_school = helper.find_top(self.grade_df, col='grade', top_cnt=self.top_cnt, best_cnt=self.best_cnt)
