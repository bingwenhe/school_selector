import connexion
from flask_cors import CORS
from exceptions import RandomNumberExhausted
from exceptions_handler import handle_random_number_exhausted_exception

# Create the application instance
app = connexion.App(__name__, specification_dir='./')

# Read the swagger.yml file to configure the endpoints
app.add_api('swagger.yml', validate_responses=True)
app.add_error_handler(RandomNumberExhausted, handle_random_number_exhausted_exception)


CORS(app.app)


if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=5000)
