import logging

from flask import jsonify
import pandas as pd

import parameter_configuration as para_conf
import helper

logging.basicConfig(level=logging.INFO)
LOGGER = logging.getLogger(__name__)

pd.set_option('display.width', 1000)
pd.set_option('display.max_rows', 15000)


def fetch_grade(method, class_year, subject):
    LOGGER.info(f"input: {method} {class_year} {subject}")
    return get_school_data(method, class_year, subject)


def get_school_grade_filename(class_year, subject):
    school_grade_filename = ""

    if class_year == para_conf.CLASS_AK6:
        if subject == 'amne':
            school_grade_filename = para_conf.DATA_AK6_AMNE['school_grade_file']
    elif class_year == para_conf.CLASS_AK9:
        if subject == 'amne':
            school_grade_filename = para_conf.DATA_AK9_AMNE['school_grade_file']
        elif subject == 'total':
            school_grade_filename = para_conf.DATA_AK9_TOTAL['school_grade_file']

    return school_grade_filename


def get_school_data(method, class_year, subject):
    school_grade_filename = get_school_grade_filename(class_year, subject)

    print(f"school_grade_filename is: {school_grade_filename}")

    school_grade_df = pd.read_csv(school_grade_filename, float_precision='round_trip')
    school_grade_df.sort_values(by='year', inplace=True)

    # print(school_grade_df)

    result_dict = {}
    for kod, value in school_grade_df.groupby("Skol-enhetskod"):
        year_list = value['year'].tolist()
        grade_list = value[method].tolist()

        try:
            school_name = helper.find_school_name_by_id(kod)
        except Exception as e:
            print(f"We got exception: {e}")
            print(kod)
            continue
        result_dict[school_name] = {
            "year_list": year_list,
            "grade_list": grade_list
        }

    return jsonify(result_dict)
