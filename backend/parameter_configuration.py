import glob
id_name_map_filename = 'school_id_name_map.json'
# target_school_filename = 'possible_target_schools'

SRC_DATA_FOLDER = 'source_data'
RESULT_FOLDER = 'results'
CLASS_AK3 = 'ak3'
CLASS_AK6 = 'ak6'
CLASS_AK9 = 'ak9'

GRADE_TYPE_SUBJECT = 'subject'
GRADE_TYPE_TOTAL = 'total'


DATA_AK6_AMNE = {
    'file_pattern': '%s/*betyg_ak6*.csv' % (SRC_DATA_FOLDER),
    'grade_col': 'Totalt.2',

    'type': GRADE_TYPE_SUBJECT,
    'class': CLASS_AK6,

    'school_best_cnt_file': RESULT_FOLDER + '/ak6_amne_school_best_cnt',
    'school_top_cnt_file': RESULT_FOLDER + '/ak6_amne_school_top_cnt',
    'target_school_file': RESULT_FOLDER + '/ak6_amne_target_schools',
    'school_grade_file': RESULT_FOLDER + '/ak6_amne_grade.csv',
    'minimum_top_cnts': 7  # This is identified MANUALLY from Counter(all_schools)!
}


DATA_AK9_AMNE = {
    'file_pattern': '%s/*slutbetyg_amne_skola*.csv' % (SRC_DATA_FOLDER),
    'grade_col': 'Betygspoäng',

    'type': GRADE_TYPE_SUBJECT,
    'class': CLASS_AK9,

    'school_best_cnt_file': RESULT_FOLDER + '/ak9_amne_school_best_cnt',
    'school_top_cnt_file': RESULT_FOLDER + '/ak9_amne_school_top_cnt',
    'target_school_file': '%s/ak9_amne_target_schools' % (RESULT_FOLDER),
    'school_grade_file': '%s/ak9_amne_grade.csv' % (RESULT_FOLDER),
    'minimum_top_cnts': 6  # This is identified MANUALLY from Counter(all_schools)!
}


DATA_AK9_TOTAL = {
    'file_pattern': '%s/*slutbetyg_skola*.csv' % (SRC_DATA_FOLDER),
    'grade_col': 'Genomsnittligt meritvärde (16)',

    'type': GRADE_TYPE_TOTAL,
    'class': CLASS_AK9,

    'school_best_cnt_file': RESULT_FOLDER + '/ak9_total_school_best_cnt',
    'school_top_cnt_file': RESULT_FOLDER + '/ak9_total_school_top_cnt',
    'target_school_file': '%s/ak9_total_target_schools' % (RESULT_FOLDER),
    'school_grade_file': '%s/ak9_total_grade.csv' % (RESULT_FOLDER),
    'minimum_top_cnts': 24  # This is identified MANUALLY from Counter(all_schools)!
}

weight_sv = 110 / 100
weight_en = 100 / 100
weight_ma = 90 / 100

# no_schools = [
#     99022046,  # Sköndalsskolan
#     88795871,  # Södra Ängby skola
#     90850620,  # Grundskolan Metapontum
#     63321041,  # Kungliga Svenska Balettskolan
#     43166282,  # Nälstaskolan
#     94677983,  # Tullgårdsskolan
# ]

# ??? 26172429 没有瑞典语成绩

no_schools = [
    92750123,  # 没有6年级了，现在 Enskilda gymnasiet, gr
]
