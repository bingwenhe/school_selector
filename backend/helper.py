import np
import parameter_configuration
import json
import numpy
from collections import Counter
import pandas as pd

BEST_CNT = 3


def numify(strg):
    if strg == '..' or strg == '.' or type(strg) is float:
        return np.nan
    else:
        return float(strg.replace(',', '.'))


# def find_top(subject, count, df):
#     sorted_df = df.loc[
#         (df['Kommun'] == 'Stockholm') &
#         (df['Ämne'] == subject)].sort_values(by='grade', ascending=False)
#     threshold = sorted_df.iloc[count]['grade']
#     return sorted_df[sorted_df['grade'] >= threshold]['Skol-enhetskod'].values.tolist()


def analyze_years_grade(final_df, min_top_cnt=None):
    all_years = final_df.columns.tolist()

    total_cnt_ds = final_df.apply(lambda x: x.count(), axis=1)
    total_cnt_ds.name = 'total_cnt'

    # Find every year top 5, and top_cnt_list
    xx = [find_top(final_df, year, BEST_CNT, min_top_cnt) for year in all_years]
    tmp_top_5_list, tmp_top_cnt_list = zip(*xx)
    best_list = flatten_list(tmp_top_5_list, allow_duplicate=False)

    if not min_top_cnt:
        return best_list

    top_cnt_list = flatten_list(tmp_top_cnt_list)

    result_cnt = Counter(top_cnt_list)
    # print(result_cnt)  # NOTE!!: use this to make sure Counter actually works!

    stat_df = pd.DataFrame.from_dict(dict(result_cnt), orient='index')
    stat_df.columns = ['top_cnt']

    stat_df = stat_df.join(total_cnt_ds, how='left')
    stat_df['perc'] = stat_df['top_cnt'] / stat_df['total_cnt'] * 100
    stat_df.sort_values('perc', inplace=True, ascending=False)

    return best_list, stat_df


def get_year_from_filename(filename):
    return str(int(filename[-11:-7]) + 1)


def flatten_list(orig_list, allow_duplicate=True):
    tmp_list = [item for x in orig_list for item in x]
    if allow_duplicate:
        return tmp_list
    else:
        return list(set(tmp_list))


# We must use grade as the threshold. Several schools might have the same grade
# Grade >= best_cnt will be directly qualified for TARGET_SCHOOL
# Grade >= top_cnt will get one point and later will be qualified by percentage comparision
def find_top(df, col, best_cnt=None, top_cnt=None):
    sorted_df = df.sort_values(by=col, ascending=False)
    threshold_best_cnt = sorted_df.iloc[best_cnt - 1][col]
    list_best_cnt = sorted_df[sorted_df[col] >= threshold_best_cnt].index.values.tolist()

    list_top_cnt = []
    if top_cnt:
        threshold_top_cnt = sorted_df.iloc[top_cnt][col]
        list_top_cnt = sorted_df[sorted_df[col] >= threshold_top_cnt].index.values.tolist()

    return list_best_cnt, list_top_cnt


def find_school_name_by_id(id):
    with open(parameter_configuration.id_name_map_filename, 'r', encoding='utf8') as file:
        xxx = json.load(file)
        name = xxx[str(id)][0]
        return name


def find_school_total_count_by_id(id, cnt_list):
    for school_id, value in cnt_list:
        if school_id == id:
            return value


class MyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, numpy.integer):
            return int(obj)
        elif isinstance(obj, numpy.floating):
            return float(obj)
        elif isinstance(obj, numpy.ndarray):
            return obj.tolist()
        else:
            return super(MyEncoder, self).default(obj)
