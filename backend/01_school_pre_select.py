import pandas as pd
import glob
import helper
import parameter_configuration as para_conf
from collections import Counter

pd.set_option('display.width', 1000)
pd.set_option('display.max_rows', 15000)
TOP_CNT = 15


csv_files_ak6_amne = glob.glob('data/*ak6*.csv')
print(csv_files_ak6_amne)
csv_files_ak9_amne = glob.glob('data/*slutbetyg_amne_skola*.csv')
print(csv_files_ak9_amne)





def get_year_from_filename(filename):
    return str(int(filename[-11:-7]) + 1)

# def find_target_school(analysis_data):
#     elif final_year == 'ak9_amne':
#         target_school_filename = para_conf.target_school_filename_ak9_amne
#         grade_col = 'Betygspoäng'
#         useCol = ['Ämne', grade_col]
#         minimum_top_cnts = 6  # This is identified MANUALLY from Counter(all_schools)!
#     elif final_year == 'ak9_total':
#         target_school_filename = para_conf.target_school_filename_ak9_total
#         grade_col = 'Genomsnittligt meritvärde (16)'
#         useCol = [grade_col]
#         minimum_top_cnts = 6  # This is identified MANUALLY from Counter(all_schools)!
#
#
#     all_csv_files = analysis_data['files']
#     final_year = analysis_data['year']
#
#
#     all_schools = []
#     for one_csv in all_csv_files:
#         one_df = pd.read_csv(one_csv, sep=';', usecols=COMMON_COL + useCol)
#         # one_df = pd.read_csv(one_csv, sep=';')
#         one_df['grade'] = one_df[grade_col].map(helper.numify)
#         print(one_df)
#
#         sv_top = helper.find_top(subject='Svenska', count=TOP_CNT, df=one_df)
#         eng_top = helper.find_top(subject='Engelska', count=TOP_CNT, df=one_df)
#         math_top = helper.find_top(subject='Matematik', count=TOP_CNT, df=one_df)
#         all_schools += sv_top + eng_top + math_top
#
#     # Todo: heatmap plot Counter(all_schools)
#     result_cnt = Counter(all_schools)
#     print(result_cnt)
#     ccc = [value for key, value in result_cnt.items()]
#     print(sorted(Counter(ccc).items()))
#     possible_target_schools = [id for id, cnt in dict(Counter(all_schools)).items() if cnt >= minimum_top_cnts]
#     with open(target_school_filename, 'w') as file:
#         for id in possible_target_schools:
#             file.write(str(int(id)) + "\n")
#

def find_target_school_by_major3(analysis_data):
    all_csv_files_path = analysis_data['file_path']
    print(all_csv_files_path)
    all_csv_files = glob.glob(all_csv_files_path)
    print(all_csv_files)
    target_school_file = analysis_data['target_school_file']
    grade_col = analysis_data['grade_col']
    minimum_top_cnts = analysis_data['minimum_top_cnts']

    all_schools = []
    all_year_df_list = []

    for csv_file in all_csv_files:
        print(csv_file)
        print(grade_col)
        one_df = pd.read_csv(csv_file, sep=';', usecols=['Kommun', 'Skol-enhetskod', 'Ämne', grade_col])
        # one_df = pd.read_csv(one_csv, sep=';')
        one_df['grade'] = one_df[grade_col].map(helper.numify)
        one_df.drop(grade_col, 1, inplace=True)
        one_df = one_df[(one_df['Kommun'] == 'Stockholm')]
        one_df = one_df.pivot_table(index='Skol-enhetskod', columns='Ämne', values='grade')
        final_df = one_df[['Svenska', 'Engelska', 'Matematik']].copy()
        final_df['year'] = get_year_from_filename(csv_file)
        all_year_df_list.append(final_df)

        sv_top = find_top(final_df, col='Svenska')
        eng_top = find_top(final_df, col='Engelska')
        math_top = find_top(final_df, col='Matematik')
        all_schools += sv_top + eng_top + math_top

    # Todo: heatmap plot Counter(all_schools)
    result_cnt = Counter(all_schools)
    print(result_cnt)
    ccc = [value for key, value in result_cnt.items()]
    print(sorted(Counter(ccc).items()))
    possible_target_schools = [id for id, cnt in dict(Counter(all_schools)).items() if cnt >= minimum_top_cnts]
    with open(target_school_file, 'w') as file:
        for id in possible_target_schools:
            file.write(str(int(id)) + "\n")


    complete_df = pd.DataFrame(all_year_df_list)
    
    with open(target_school_file, 'w') as file:
        for id in possible_target_schools:
            file.write(str(int(id)) + "\n")

find_target_school_by_major3(para_conf.DATA_AK6_AMNE)
# find_target_school_by_major3(para_conf.DATA_AK9_AMNE)
# find_target_school_by_major3(para_conf.DATA_AK9_TOTAL)
