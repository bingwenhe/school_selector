import pandas as pd
import glob
from src import helper, parameter_configuration

# Always run this the command before at the start of notebook

pd.set_option('display.width', 1000)
pd.set_option('display.max_rows', 15000)

csv_files_ak6 = glob.glob('data/*ak6*.csv')
print(csv_files_ak6)
csv_files_ak9_amne = glob.glob('data/*slutbetyg_amne_skola*.csv')
print(csv_files_ak9_amne)
csv_files_ak9_total = glob.glob('data/*slutbetyg_skola*.csv')
print(csv_files_ak9_total)


def calculate_school_grades(all_csv_files, final_year):
    if final_year == 'ak6':
        target_school_filename = parameter_configuration.target_school_filename_ak6
        school_grade_filename = parameter_configuration.school_grade_ak6
        grade_column = 'Totalt.2'
    elif final_year == 'ak9_amne':
        target_school_filename = parameter_configuration.target_school_filename_ak9_amne
        school_grade_filename = parameter_configuration.school_grade_ak9_amne
        grade_column = 'Betygspoäng'

    all_schools = []
    with open(target_school_filename, 'r') as file:
        for line in file:
            all_schools.append(line.rstrip())
    print("here 01")

    all_df_list = []
    for one_csv in all_csv_files:
        one_df = pd.read_csv(one_csv, sep=';',  dtype=object, usecols=['Skol-enhetskod', 'Ämne', grade_column])
        one_df['grade'] = one_df[grade_column].map(helper.numify)
        year = '20' + one_csv[-6:-4]
        one_df['year'] = int(year)
        all_df_list.append(one_df)
    print("here 02")
    final_df = pd.concat(all_df_list)
    final_df = final_df[
        (final_df['Skol-enhetskod'].isin(all_schools)) &
        (final_df['Ämne'].isin(['Svenska', 'Engelska', 'Matematik']))]
    final_df = final_df[final_df['Skol-enhetskod'] != '26172429']
    final_df = final_df.pivot_table(index=['Skol-enhetskod', 'year'], columns='Ämne')
    final_df.columns = final_df.columns.get_level_values(1)
    final_df.columns.name = None

    def calc_sum(x):
        x['simple_sum'] = x['Engelska'] + x['Matematik'] + x['Svenska']
        x['bingwen_sum'] = x['Engelska'] * parameter_configuration.weight_en + \
                           x['Matematik'] * parameter_configuration.weight_ma + \
                           x['Svenska'] * parameter_configuration.weight_sv
        return x

    xx = final_df.apply(lambda x: calc_sum(x), axis=1)
    print(xx)
    for k, v in xx.groupby('Skol-enhetskod'):
        if len(v.index.get_level_values('year')) != 5:
            print(k)
            print(v.index.get_level_values('year'))
    xx.to_csv(school_grade_filename, float_format='%.1f')


def calculate_school_grades_ak9_total(analysis_data):
    all_csv_files = analysis_data['files']
    target_school_file = analysis_data['target_school_file']
    grade_col = analysis_data['grade_col']

    all_schools = []
    with open(target_school_file, 'r') as file:
        for line in file:
            all_schools.append(line.rstrip())

    all_year_df_list = []
    for csv_file in all_csv_files:
        one_df = pd.read_csv(csv_file, sep=';', usecols=['Kommun', 'Skol-enhetskod', grade_col])
        print(one_df.head(10))
        one_df['grade'] = one_df[grade_col].map(helper.numify)
        one_df.drop(grade_col, 1, inplace=True)
        one_df = one_df[(one_df['Kommun'] == 'Stockholm')].copy()
        one_df['year'] = get_year(csv_file)
        one_df.drop('Kommun', 1, inplace=True)
        all_year_df_list.append(one_df)
    final_df = pd.concat(all_year_df_list)
    final_df = final_df.pivot_table(index='Skol-enhetskod', columns='year', values='grade')
    final_df.dropna(how='all', inplace=True)
    final_df['total_cnt'] = final_df.apply(lambda x: x.count(), axis=1)
    print(final_df.head(10))


# calculate_school_grades(csv_files_ak6,  'ak6')
# calculate_school_grades(csv_files_ak9_amne,  'ak9_amne')
calculate_school_grades_ak9_total(parameter_configuration.DATA_AK9_TOTAL)
