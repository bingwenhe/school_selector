class RandomNumberExhausted(Exception):
    status_code = 204

    def __init__(self):
        Exception.__init__(self)
        self.message = "No more random number can be generated from this seed number. "
        self.message += "Try with another number please."

    def to_dict(self):
        return {
            'message': self.message
        }
