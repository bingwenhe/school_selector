import sys
import os
new_path = os.path.join(os.path.dirname(__file__), "..")
new_path = os.path.join(new_path, '..')
sys.path.append(new_path)
print(sys.path)
import pytest
import api
import exceptions


def test_init():
    rne = exceptions.RandomNumberExhausted()
    assert rne.message == "No more random number can be generated from this seed number. Try with another number please."


def test_to_dict():
    rne = exceptions.RandomNumberExhausted()
    assert rne.to_dict()['message'] == "No more random number can be generated from this seed number. Try with another number please."
