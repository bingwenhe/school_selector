import glob
import pandas as pd
import sys
import io
import os
import json
from collections import defaultdict

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf8')

current_path = os.path.dirname(os.path.abspath(__file__))
source_data_path = os.path.join(os.path.join(current_path, ".."), "source_data")
map_file = os.path.join(os.path.join(current_path, ".."), "school_id_name_map.json")
source_data_pattern = os.path.join(source_data_path, "*.csv")
csv_files = glob.glob(source_data_pattern)


def clean_up_final_dict(final_dict):
    for key, value in final_dict.items():
        if len(value) == 2:
            if value[0].lower() == value[1].lower():
                final_dict[key] = [value[0].lower()]
                continue
    return final_dict


def get_mapping_one_file(csv_file):
    print(csv_file)

    def check_col(col_name):
        if col_name in ['Skol-enhetskod', 'Skola', 'Skolnamn']:
            return True
        return False

    one_df = pd.read_csv(csv_file, sep=';', usecols=check_col)

    if 'Skolnamn' in one_df.columns:
        one_df = one_df.rename(columns={'Skolnamn': 'Skola'})

    one_df['Kod'] = one_df['Skol-enhetskod'].astype(str)
    one_df.drop(['Skol-enhetskod'], 1, inplace=True)
    return one_df


all_df = [get_mapping_one_file(csv_file) for csv_file in csv_files]

final_df = pd.concat(all_df)
final_df.drop_duplicates(inplace=True)
print(final_df.head(10))
print(final_df.shape)


final_dict = defaultdict(list)


def collect_mapping(row):
    kod = row['Kod']
    skola = row['Skola']
    final_dict[kod].append(skola)


final_df.apply(lambda row: collect_mapping(row), axis=1)

clean_up_final_dict(final_dict)

print("Map file: ", map_file)
print(final_dict)
with open(map_file, 'w') as map_json:
    json.dump(final_dict, map_json)

    # fieldnames = ['count', 'school_kod', 'school_name']
    # writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
    # writer.writeheader()
    #
    # def write_row(row):
    #     writer.writerow({'school_kod': school_id, 'school_name': helper.find_school_name_by_id(school_id)})
    #
    #     print(row['Skola'])
    #     print(row['Kod'])
    #
    # final_df.apply(lambda row: write_row(row), axis=1)
    # for school_id, value in final_df.to_dict():
    #     print(school_id)
