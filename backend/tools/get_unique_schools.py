import sys
import csv

ALL_FILES = sys.argv[1:]


def collect_schools_in_one_file(file_name):
    print("Working on ", file_name)
    with open(file_name, 'r') as file_handle:
        reader = csv.DictReader(file_handle)
        return [row['school_name'] for row in reader]


all_schools = []
for file_name in ALL_FILES:
    all_schools += collect_schools_in_one_file(file_name)

for one_school in set(all_schools):
    print(one_school)
