from flask import jsonify


def handle_random_number_exhausted_exception(e):
    message_dict = e.to_dict()
    value_dict = {
        "random_number": None,
        "is_prime": None
    }
    message_dict.update(value_dict)
    response = jsonify(message_dict)
    print(response.data)
    return response
